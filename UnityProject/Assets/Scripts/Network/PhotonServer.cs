﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PhotonServer : Singleton<PhotonServer>
{
    public static event Action OnGameStarted = () => { };

    private bool waitForSecondPlayer = false;

    public void OnPhotonPlayerDisconnected(PhotonPlayer player)
    {
        PhotonNetwork.Disconnect();
        PlayerLeftMenu.Show();
    }

    public void Connect()
    {
        AsyncOperation op = SceneManager.LoadSceneAsync("Game", LoadSceneMode.Additive);

        op.completed += GameSceneLoaded;
    }

    private void GameSceneLoaded(AsyncOperation op)
    {
        PhotonNetwork.ConnectUsingSettings("ping_pong_1.0");
    }

    private void OnConnectedToMaster()
    {
        PhotonNetwork.JoinLobby();
    }

    private void OnJoinedLobby()
    {
        RoomOptions options = new RoomOptions();

        options.MaxPlayers = 2;

        PhotonNetwork.JoinOrCreateRoom("Room 1", options, TypedLobby.Default);
    }

    private void OnJoinedRoom()
    {
        waitForSecondPlayer = true;
    }

    private void Update()
    {
        if (waitForSecondPlayer && PhotonNetwork.room != null && PhotonNetwork.room.PlayerCount > 1)
        {
            StartGame();
        }
    }

    public void StartGame()
    {
        waitForSecondPlayer = false;

        OnGameStarted();
    }

    public void PlayOffline()
    {
        PhotonNetwork.Disconnect();
        PhotonNetwork.isMessageQueueRunning = false;
        PhotonNetwork.offlineMode = true;

        OnGameStarted();
    }
}