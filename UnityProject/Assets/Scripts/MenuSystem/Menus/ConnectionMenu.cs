﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class ConnectionMenu : SimpleMenu<ConnectionMenu>
{
    public Text connecting;
    public Text searching;

    public Button playOffline;

    private void Start()
    {
        playOffline.onClick.AddListener(OnPlayOfflineClick);

        connecting.gameObject.SetActive(true);
        searching.gameObject.SetActive(false);

        PhotonServer.OnGameStarted += PhotonServer_OnGameStarted;
    }

    private void PhotonServer_OnGameStarted()
    {
        Close();
        MainMenu.Hide();
    }

    protected override void OnDestroy()
    {
        base.OnDestroy();

        PhotonServer.OnGameStarted -= PhotonServer_OnGameStarted;
    }

    private void OnConnectedToPhoton()
    {
        connecting.gameObject.SetActive(false);
        searching.gameObject.SetActive(true);
    }

    private void OnPlayOfflineClick()
    {
        PhotonServer.Instance.PlayOffline();
    }
}