﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MainMenu : SimpleMenu<MainMenu>
{
    public Button playButton;

    private void Start()
    {
        playButton.onClick.AddListener(OnPlayButtonClick);
    }

    private void OnPlayButtonClick()
    {
        ConnectionMenu.Show();

        PhotonServer.Instance.Connect();
    }
}