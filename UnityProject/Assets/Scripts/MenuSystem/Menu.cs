﻿using UnityEngine;

public abstract class Menu<T> : Menu where T : Menu<T>
{
    public static T Instance { get; private set; }

    protected virtual void Awake()
    {
        Instance = (T)this;
    }

    protected virtual void OnDestroy()
    {
        Instance = null;
	}
    
    protected static void Open()
	{
		if (Instance == null)
			MenuManager.Instance.CreateInstance<T>();
		else
			Instance.gameObject.SetActive(true);
        
        if (Instance.IgnoreMenuManager == false)
            MenuManager.Instance.OpenMenu(Instance);

        Instance.OnOpened();
	}

	protected static void Close()
	{
		if (Instance == null)
		{
			Debug.LogWarningFormat("Trying to close menu {0} but Instance is null", typeof(T));
			return;
		}

        if (Instance.IgnoreMenuManager == false)
            MenuManager.Instance.CloseMenu(Instance);
        else
            Destroy(Instance.gameObject);
	}

	public override void OnBackPressed()
	{
		Close();
	}

    protected static void LockCursor()
    {
#if !MOBILE_INPUT
        Cursor.lockState = CursorLockMode.Locked;
        Cursor.visible = false;
#endif
    }

    protected static void ShowCursor()
    {
#if !MOBILE_INPUT
        Cursor.lockState = CursorLockMode.None;
        Cursor.visible = true;
#endif
    }
}

public abstract class Menu : MonoBehaviour
{
	[Tooltip("Destroy the Game Object when menu is closed (reduces memory usage)")]
	public bool DestroyWhenClosed = true;

	[Tooltip("Disable menus that are under this one in the stack")]
	public bool DisableMenusUnderneath = true;

    [Tooltip("Disable when open another")]
    public bool IgnoreMenuManager = false;

    public abstract void OnBackPressed();
    public virtual void OnOpened() { }
    public virtual void UpdateState() { }
}
