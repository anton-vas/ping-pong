﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BallMovement : Photon.MonoBehaviour
{
    public float speed;

    private new Rigidbody2D rigidbody;

    private void Awake()
    {
        rigidbody = GetComponent<Rigidbody2D>();
    }

    public void RandomDirection()
    {
        if (photonView.isMine == false && !PhotonNetwork.offlineMode)
        {
            return;
        }

        Vector2 force = new Vector2(Random.Range(-0.25f, 0.25f), Random.Range(-1f, 1));

        if (!PhotonNetwork.offlineMode)
            photonView.RPC("SetDirection", PhotonTargets.All, force, speed);
        else
            SetDirection(force, speed);
    }

    public void ResetState()
    {
        if (photonView.isMine == false && !PhotonNetwork.offlineMode)
        {
            return;
        }

        if (!PhotonNetwork.offlineMode)
            photonView.RPC("ResetPosition", PhotonTargets.All);
        else
            ResetPosition();
    }

    [PunRPC]
    private void SetDirection(Vector2 force, float speed)
    {
        rigidbody.AddForce(force.normalized * speed);
    }

    [PunRPC]
    private void ResetPosition()
    {
        transform.position = Vector2.zero;
        rigidbody.velocity = Vector2.zero;
    } 
}