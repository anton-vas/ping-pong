﻿using UnityEngine;
using System.Collections;
using System;
using Random = UnityEngine.Random;

[CreateAssetMenu]
public class BallDataArray : ScriptableObject
{
    public BallData[] array;

    public int GetRandomIndex()
    {
        if (array.Length == 0)
        {
            throw new Exception("Balls array is empty.");
        }

        return Random.Range(0, array.Length);
    }
}