﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu]
public class BallData : ScriptableObject
{
    public Color color;
    public float speed;
}