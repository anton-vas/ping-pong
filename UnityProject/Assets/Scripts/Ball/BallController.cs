﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BallController : Photon.MonoBehaviour
{
    public BallDataArray ballsArray;

    private SpriteRenderer ballRenderer;
    private BallMovement movement;

    private void Awake()
    {
        movement = GetComponent<BallMovement>();
        ballRenderer = GetComponent<SpriteRenderer>();
    }
    
    private IEnumerator Start()
    {
        while (!PhotonNetwork.offlineMode && PhotonNetwork.inRoom == false
            || !PhotonNetwork.offlineMode && PhotonNetwork.room.PlayerCount == 1)
            yield return null;

        movement.RandomDirection();

        SetupParameters();
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (photonView.isMine == false)
        {
            return;
        }

        if (collision.gameObject.CompareTag("Horizontal"))
        {
            movement.ResetState();
            movement.RandomDirection();

            if (!PhotonNetwork.offlineMode)
                photonView.RPC("SetupParameters", PhotonTargets.All, ballsArray.GetRandomIndex());
            else
                SetupParameters(ballsArray.GetRandomIndex());
        }
    }

    [PunRPC]
    private void SetupParameters(int index = 0)
    {
        BallData ballData = ballsArray.array[index];

        movement.speed = ballData.speed;
        ballRenderer.color = ballData.color;
    }
}