﻿using UnityEngine;
using System.Collections;

public class PlayerSync : Photon.MonoBehaviour
{
    private PlayerInput playerInput;
    private PlayerMovement playerMovement;

    private void Awake()
    {
        playerInput = GetComponent<PlayerInput>();
        playerMovement = GetComponent<PlayerMovement>();
    }

    private void Start()
    {
        playerInput.enabled = photonView.isMine;
    }

    private void OnPhotonSerializeView(PhotonStream stream, PhotonMessageInfo info)
    {
        if (stream.isWriting)
        {
            stream.SendNext(playerMovement.posX);
        }
        else
        {
            playerMovement.posX = (float)stream.ReceiveNext();
        }
    }
}