﻿using UnityEngine;
using System.Collections;

public class PlayerSpawner : MonoBehaviour
{
    public Transform firstPlayerPosition;
    public Transform secondPlayerPosition;
    
    private void Awake()
    {
        PhotonServer.OnGameStarted += OnGameStarted;

        // Test case
        if (PhotonServer.IsExist() == false)
        {
            PhotonNetwork.offlineMode = true;

            OnGameStarted();
        }
    }

    private void OnGameStarted()
    {
        SpawnPlayer();
    }

    private void SpawnPlayer()
    {
        if (PhotonNetwork.offlineMode == false)
        {
            bool firstPlayer = PhotonNetwork.isMasterClient;

            Transform spawnPoint = firstPlayer ? firstPlayerPosition : secondPlayerPosition;

            PhotonNetwork.Instantiate("Player", spawnPoint.position, Quaternion.identity, 0);
        }
        else
        {
            GameObject playerPrefab = Resources.Load<GameObject>("Player");

            Instantiate(playerPrefab, firstPlayerPosition.position, Quaternion.identity);
            Instantiate(playerPrefab, secondPlayerPosition.position, Quaternion.identity);
        }
    }
}