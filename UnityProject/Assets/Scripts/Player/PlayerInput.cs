﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerInput : MonoBehaviour
{
    private PlayerMovement movement;

    private void Awake()
    {
        movement = GetComponent<PlayerMovement>();
    }

    private void Update()
    {
#if UNITY_EDITOR
        if (Input.GetMouseButton(0))
        {
            CheckTouch(Input.mousePosition);
        }
#endif
        
        for (int i = 0; i < Input.touchCount; i++)
        {
            CheckTouch(Input.GetTouch(i).position);
        }
    }

    private void CheckTouch(Vector2 position)
    {
        Vector2 worldPos = Camera.main.ScreenToWorldPoint(position);

        if (worldPos.y < 0 && transform.position.y < 0 ||
            worldPos.y > 0 && transform.position.y > 0)
        {

            movement.posX = worldPos.x;
        }
    }
}