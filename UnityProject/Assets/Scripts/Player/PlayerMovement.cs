﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour
{
    public float posX;

    private float lerpedPosX;

    private float positionConstraintX;

    private void Awake()
    {
        FindClosestWall();
    }

    private void FindClosestWall()
    {
        float width = GetComponent<SpriteRenderer>().bounds.size.x;

        Vector2 origin = new Vector2(transform.position.x - width / 1.9f, transform.position.y);
        
        RaycastHit2D hit = Physics2D.Raycast(origin, Vector2.left, 5);

        positionConstraintX = hit.point.x + width / 2;
    }

    private void Update()
    {
        posX = Mathf.Clamp(posX, positionConstraintX, -positionConstraintX);

        lerpedPosX = Mathf.Lerp(lerpedPosX, posX, Time.deltaTime * 10);

        transform.position = new Vector2(lerpedPosX, transform.position.y);
    }
}