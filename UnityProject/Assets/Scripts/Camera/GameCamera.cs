﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameCamera : MonoBehaviour
{
    private void OnJoinedRoom()
    {
        FlipCamera();
    }

    private void FlipCamera()
    {
        bool firstPlayer = PhotonNetwork.isMasterClient;

        if (firstPlayer == false)
            transform.localEulerAngles = new Vector3(0, 0, 180);
    }
}
