﻿using UnityEngine;

public class Singleton<T> : MonoBehaviour where T : MonoBehaviour
{
    protected static T instance;
    
    public static T Instance
    {
        get
        {
            if (instance == null)
            {
                instance = GetInstance();

                if (instance == null)
                {
                    Debug.LogError("В сцене нужен экземпляр " + typeof(T) +
                       ", но он отсутствует.");
                }
            }

            return instance;
        }
    }

    public static bool IsExist()
    {
        if (instance != null)
            return true;

        return GetInstance() != null;
    }

    private static T GetInstance()
    {
        return (T)FindObjectOfType(typeof(T));
    }
}